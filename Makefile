CC = gcc
CFLAGS = -Wall -g3
INCLUDES = -Iinclude

SOURCEDIR = src/

SRCS = $(wildcard $(SOURCEDIR)/*.c)
OBJS = $(SRCS:.c=.o)
BIN = Sldx
RM = rm -f

all: $(SRCS) $(BIN)

$(BIN): $(OBJS)
	$(CC) $(CFLAGS) $(INCLUDES) -o $(BIN) $(OBJS)

.c.o:
	$(CC) $(CFLAGS) $(INCLUDES) -c $<  -o $@


clean:
	${RM} *~ ${BIN} ${OBJS}
