#ifndef ALG_X_H
#define ALG_X_H

#include "DancingLinks.h"

void algXSearch(struct Hnode *dlinks, int k, struct Dnode **O);
void cover(struct Hnode *tocover);
void uncover(struct Hnode *touncover);

//extern void foundSolution(struct Dnode **sol, int len);
void (*foundSolution)(struct Dnode **sol, int len);

#endif // ALG_X_H
