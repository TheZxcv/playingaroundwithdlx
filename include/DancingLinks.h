#ifndef DANCING_LINKS_H
#define DANCING_LINKS_H

#include <stdio.h>
#include <stdlib.h>

struct Dnode {
	struct Dnode *right;
	struct Dnode *left;
	void *up;
	void *down;

	struct Hnode *header;
	int ID;
};

struct Hnode {
	struct Hnode *left;
	struct Hnode *right;
	
	struct Dnode *up;
	struct Dnode *down;

	char *name;
	int size;	// how many ones
};
/* Makes a sparse matrix from a normal one 
 * 	p_cols: how many columns must have
 * 		one and only one 1.
 */
struct Hnode *matrixToDancingLinks(int *matr, int rows, int cols, int p_cols);
void printHnode(struct Hnode *n);
void printDnode(struct Dnode *n);

char *(*genHeaderName)(int col);

#endif // DANCING_LINKS_H
