#include "DancingLinks.h"


#define A2M(x, y) ((x)*cols+(y))
// p_cols: primary columns, if 0 every column is primary
struct Hnode *matrixToDancingLinks(int *matr, int rows, int cols, int p_cols) {
	struct Hnode *header = malloc(sizeof(struct Hnode)*(cols + 1));
	if(p_cols < 1)
		p_cols = cols;
	header->name = "H";
	header->size = 0;
	header->up = header->down = NULL;
	header->right = header + 1;
	header->left = header + p_cols;

	struct Hnode *temp = header+1;
	int i = 0;
	for(; i<cols; i++) {
		temp->name = genHeaderName(i);
		temp->size = 0;
		temp->up = temp->down = (void *) temp;
		if(p_cols > 0) {
			temp->left = temp - 1;
			temp->right = temp + 1;
			p_cols--;
		} else
			temp->left = temp->right = temp;
		temp++;
	}
	header->left->right = header;

	temp = header->right;
	int j;
	for(i=0; i<rows; i++) {
		struct Dnode *node = NULL;
		struct Dnode *fnode = NULL;
		struct Dnode *lnode = NULL;
		for(j=0; j<cols; j++) {
			if(matr[A2M(i, j)]) {
				node = malloc(sizeof(struct Dnode));
				node->ID = A2M(i, j);
				temp->size++;
				node->header = temp;

				if(fnode == NULL)
					fnode = node;
				node->left = lnode;
				if(lnode != NULL)
					lnode->right = node;

				if(temp->down == (void *) temp) {
					temp->down = node;
					node->up = temp;
				} else {
					struct Dnode *u = temp->down;
					while(u->down != temp)
						u = u->down;
					u->down = node;
					node->up = u;
				}	
				node->down = temp;
				temp->up = node;
				lnode = node;
			}
			temp++;
		}
		if(fnode != NULL) {
			fnode->left = node;
			node->right = fnode;
		}
		temp = header+1;
	}
	return header;
}

void printHnode(struct Hnode *n) {
	printf("%s - %8p:\n", n->name, n);
	printf("\t s:%d\n", n->size);
	printf("\t u:%8p\n", n->up);
	printf("\t d:%8p\n", n->down);
	printf("\t r:%8p\n", n->right);
	printf("\t l:%8p\n", n->left);
}

void printDnode(struct Dnode *n) {
	if(n->header != NULL) {
		printf("%s %04d - %8p:\n", n->header->name, n->ID, n);
		printf("\t s:%d\n", n->header->size);
	} else
		printf("%8p %04d - %8p:\n", n->header, n->ID, n);
	printf("\t u:%8p\n", n->up);
	printf("\t d:%8p\n", n->down);
	printf("\t r:%8p\n", n->right);
	printf("\t l:%8p\n", n->left);
}
