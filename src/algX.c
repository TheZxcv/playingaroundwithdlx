#include "algX.h"

void algXSearch(struct Hnode *dlinks, int k, struct Dnode **O) {
	if(dlinks->right == dlinks) {
		foundSolution(O, k);
		return;
	}

	struct Hnode *column = dlinks->right;
	struct Hnode *temp = dlinks->right->right;
	while(temp != dlinks) {
		if(column->size > temp->size)
			column = temp;
		temp = temp->right;
	}

	cover(column);
	
	struct Dnode *node = column->down;
	while((void *) node != (void *) column) {
		O[k] = node;
		struct Dnode *rnode = node->right;
		while(rnode != node) {
			cover(rnode->header);
			rnode = rnode->right;
		}
		algXSearch(dlinks, k+1, O);
		node = O[k];
		column = node->header;

		struct Dnode *lnode = node->left;
		while(lnode != node) {
			uncover(lnode->header);
			lnode = lnode->left;
		}
		node = node->down;
	}
	uncover(column);
}

void cover(struct Hnode *tocover) {
//	printf("Covering: ");
//	printHnode(tocover);

	tocover->right->left = tocover->left;
	tocover->left->right = tocover->right;

	struct Dnode *node = tocover->down;
	while((void *) node != (void *) tocover) {
		struct Dnode *rnode = node->right;
		while(rnode != node) {
			void *temp = rnode->down;
			if(temp == rnode->header)
				((struct Hnode *) temp)->up = rnode->up;
			else
				((struct Dnode *) temp)->up = rnode->up;
			
			temp = rnode->up;
			if(temp == rnode->header)
				((struct Hnode *) temp)->down = rnode->down;
			else
				((struct Dnode *) temp)->down = rnode->down;
						
			rnode->header->size--;
			rnode = rnode->right;
		}
		node = node->down;
	}
}


void uncover(struct Hnode *touncover) {
	//printf("Uncovering: ");
	//printHnode(touncover);

	struct Dnode *node = touncover->up;
	while((void *) node != (void *) touncover) {
		struct Dnode *lnode = node->left;
		while(lnode != node) {
			lnode->header->size++;

			void *temp = lnode->down;
			if(temp == lnode->header)
				((struct Hnode *) temp)->up = lnode;
			else
				((struct Dnode *) temp)->up = lnode;
			
			temp = lnode->up;
			if(temp == lnode->header)
				((struct Hnode *) temp)->down = lnode;
			else
				((struct Dnode *) temp)->down = lnode;
				
			lnode = lnode->left;
		}
		node = node->up;
	}

	touncover->right->left = touncover;
	touncover->left->right = touncover;
}
