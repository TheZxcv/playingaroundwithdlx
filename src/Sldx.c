#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "DancingLinks.h"
#include "algX.h"

#define ROWS 729		//9*81
#define COLUMNS 324		//4*81

int solutions;

int *genMatrix(int g[][9]);
void printSudoku(int **grid);
int **nodesToGrid(struct Dnode **sol, int len);

void printSolution(struct Dnode **sol, int len) {
	solutions++;
	int i = 0;
	for(; i<len; ++i) {
		struct Dnode *node = sol[i];
		do {
			printf("%s", node->header->name);
			node = node->right;
		} while(node != sol[i]);
		putchar('\n');
	}
//	getchar();
}

void printFoundGrid(struct Dnode **sol, int len) {
	solutions++;
	printSudoku(nodesToGrid(sol, len));
//	getchar();
}

char *genName(int col) {
	char c;
	int i;
	if(col < 81 ) {
		c = 'P';
		i = col;
	} else if(col < 81*2) {
		c = 'R';
		i = col - 81;
	} else if(col < 81*3) {
		c = 'C';
		i = col - 81*2;
	} else {
		c = 'S';
		i = col - 81*3;
	}
	char *str = malloc(sizeof(char)*4);
	sprintf(str, "%c%02d", c, i);
	return str;
}


int main (void) {
	int g[][9] = {
			{7, 0, 3, 1, 5, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0},
			{9, 0, 0, 2, 0, 0, 0, 0, 4},
			{6, 0, 0, 0, 9, 0, 5, 0, 0},
			{0, 0, 8, 5, 3, 1, 7, 0, 0},
			{0, 0, 1, 0, 4, 0, 0, 0, 3},
			{5, 0, 0, 0, 0, 6, 0, 0, 9},
			{0, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 7, 8, 3, 0, 2}
		};
	foundSolution = printFoundGrid;
	genHeaderName = genName;
	int *m = genMatrix(g);
	struct Hnode *dl = matrixToDancingLinks(m, ROWS, COLUMNS, -1);
	solutions = 0;
	algXSearch(dl, 0, calloc(100, sizeof(struct Dnode *)));
	printf("Solutions: %d\n", solutions);
	return EXIT_SUCCESS;

}

int *genMatrix(int g[][9]) {
	int *m = calloc(ROWS*COLUMNS, sizeof(int));

	int i;
	for(i=0; i<ROWS; i++) {
		if(g[(i/9)/9][(i/9)%9] == 0 || g[(i/9)/9][(i/9)%9] == i%9 + 1) {
			/* first constraint */
			m[i*COLUMNS + i/9] = 1;

			/* second: rows */
			m[i*COLUMNS + 81 + (i/81)*9 + i%9] = 1;

			/* third: columns */
			m[i*COLUMNS + 81*2 + i%81] = 1;

			/* fourth: squares */
			m[i*COLUMNS + 81*3 + (((i/9)/3)%3 + (((i/9)/9)/3)*3)*9 + i%9] = 1;
		}
	}
	return m;
}

void printSudoku(int **grid) {
	int i, j, k;
	for(i=0; i<9; ++i) {
		if(i%3 == 0) {
			for(k=0; k<9; ++k) {
				if(k%3 == 0)
					printf("+");
				printf("-");
			}
			printf("+\n");
		}	
		for(j=0; j<9; ++j) {
			if(j%3 == 0)
				printf("|");
			if(grid[i][j] == 0)
				printf(" ");
			else
				printf("%d", grid[i][j]);
		}
		printf("|\n");
	}
	
	for(i=0; i<9; ++i) {
		if(i%3 == 0)
			printf("+");
		printf("-");
	}
	printf("+\n");
}


int **nodesToGrid(struct Dnode **sol, int len) {
	assert(len == 81);
	int **g = malloc(sizeof(int *) * 9);

	int i;
	for(i=0;i < 9; i++)
		g[i] = malloc(sizeof(int)*9);

	while(len-- > 0) {
		struct Dnode *node = *sol++;
		int p, n;
		p = n = -1;
		do {
			char c;
			int t;
			//printHnode(node->header);
			sscanf(node->header->name, "%c%02d", &c, &t);
			//printf("c:%c, t:%d\n", c, t);
			switch(c) {
				case 'P':
					p = t;
					break;
				case 'R':
				case 'C':
				case 'S':
					n = t%9 + 1;
					break;
				default:
					break;
			}
			if(p != -1 && n != -1)
				break;
			node = node->right;
		} while(node != *sol);
		//printf("i: %d, j: %d, n: %d\n", p/9, p%9, n);
		g[p/9][p%9] = n;
	}
	return g;
}
