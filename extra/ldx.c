#include <stdio.h>
#include <stdlib.h>

#include "DancingLinks.h"
#include "algX.h"

#define ROWS 54			//9*6
#define COLUMNS 57		//9 + 3*6 + 3*6 + 2*6

int solutions;

int *genMatrix(void);

char *genName(int col) {
	char c;
	int i;
	if(col < 9 ) {
		c = 'P';
		i = col;
	} else if(col < 27) {
		c = 'R';
		i = col-9;
	} else if(col < 45) {
		c = 'C';
		i = col-27;
	} else {
		c = 'D';
		i = col-45;
	}
	char *str = malloc(sizeof(char)*4);
	sprintf(str, "%c%02d", c, i);
	return str;
}

void printGrid(struct Dnode **sol, int len) {
	solutions++;
	
	int m[9];
	int i = 0;
	char c;
	int tmp;
	int p = -1;
	int n = -1;
	for(; i<len; ++i) {
		struct Dnode *node = sol[i];
		while(n == -1 || p == -1) {
			sscanf(node->header->name, "%c%04d", &c, &tmp);
			if(c == 'P')
				p = tmp;
			else
				n = tmp%6;
			node = node->right;
		}
		m[p] = n;
		p = n = -1;
	}
	printf("+-+-+-+\n");
	printf("|%d|%d|%d|\n", m[0], m[1], m[2]); 
	printf("|%d|%d|%d|\n", m[3], m[4], m[5]); 
	printf("|%d|%d|%d|\n", m[6], m[7], m[8]); 
	printf("+-+-+-+\n");
//	getchar();	
}

int main (void) {
	foundSolution = printGrid;
	genHeaderName = genName;
	int *m = genMatrix();
	struct Hnode *dl = matrixToDancingLinks(m, ROWS, COLUMNS, 9);
	solutions = 0;
	algXSearch(dl, 0, calloc(20, sizeof(struct Dnode *)));
	printf("Solutions: %d\n", solutions);
	return EXIT_SUCCESS;

}

int *genMatrix(void) {
	int *matr = calloc(ROWS*COLUMNS, sizeof(int));

	int i,j;
	for(i=0; i<ROWS; ++i) {
		int offset = i*COLUMNS;
		matr[offset + i/6] = 1;
		matr[offset + 9 + (i/18)*6 + i%6] = 1;
		matr[offset + 27 + i%18] = 1;
		
		int r = i/18;
		int c = (i/6)%3;
		if(r == c)
			matr[offset + 45 + i%6] = 1;
		if(r+c == 2)
			matr[offset + 45 + 6 + i%6] = 1;
	}
/*	int TOT = 0;
	printf("|  |012345678|000000111111222222|000000111111222222|000000111111|\n");
	for(i=0; i<ROWS*COLUMNS; ++i) {
		switch(i%columns) {
			case 0:
				printf("|%02d|", i/COLUMNS);
				break;
			case 9:
			case 27:
			case 45:
				printf("|");
				break;
			default:
				break;
		}
		if(matr[i]) { TOT++;
			printf("%d", matr[i]);
		} else
			putchar(' ');
		if(i%COLUMNS == COLUMNS-1)
			printf("|\n");
	}
	putchar('\n');
	printf("Tot ones: %d\n", TOT);
*/
	return matr;
} 
