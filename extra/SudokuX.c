#include <stdio.h>
#include <stdlib.h>

#define ROWS 729		//9*81
#define COLUMNS 324		//4*81


int *genMatrix(int[][9]);
void printFrame(void) {
	int i=0;
	for(; i<COLUMNS; i++) {
		if(i%81 == 0)
			putchar('+');
		putchar('-');
	}
	putchar('+');
	putchar('\n');
}
int main(void) {
	int g[][9] = {
			{7, 0, 3, 1, 5, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0},
			{9, 0, 0, 2, 0, 0, 0, 0, 4},
			{6, 0, 0, 0, 9, 0, 5, 0, 0},
			{0, 0, 8, 5, 3, 1, 7, 0, 0},
			{0, 0, 1, 0, 4, 0, 0, 0, 3},
			{5, 0, 0, 0, 0, 6, 0, 0, 9},
			{0, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 7, 8, 3, 0, 2}
		};
	int *matr = genMatrix(g);
	int *m = matr;
	int i,j;
	for(i=0; i<ROWS; i++) {
		if(i%9 == 0) 
			printFrame();
		for(j=0; j<COLUMNS; j++) {
			if(j%81 == 0)
				putchar('|');
			printf("%d", m[i*COLUMNS + j]);
		}
		putchar('|');
		putchar('\n');
	}
	return EXIT_SUCCESS;
}

int *genMatrix(int g[][9]) {
	int *m = calloc(ROWS*COLUMNS, sizeof(int));

	int i;
	for(i=0; i<ROWS; i++) {
		if(g[(i/9)/9][(i/9)%9] == 0 || g[(i/9)/9][(i/9)%9] == i%9 + 1) {
			/* first constraint */
			m[i*COLUMNS + i/9] = 1;

			/* second: rows */
			m[i*COLUMNS + 81 + (i/81)*9 + i%9] = 1;

			/* third: columns */
			m[i*COLUMNS + 81*2 + i%81] = 1;

			/* fourth: squares */
			m[i*COLUMNS + 81*3 + (((i/9)/3)%3 + (((i/9)/9)/3)*3)*9 + i%9] = 1;
		}
	}
	return m;
}
